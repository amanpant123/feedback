import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
const data=[
    {  id:'1',
       question:'Q1. Want to know about steps to download Facebook/Instagram videos?',
       answer:{  
           line1:'You can easily download the videos by following this steps:',
           line2:'1. Copy the URL of the video you want to download.',
           line3:'2. Open the Homepage of the Rocks Video Player and paste the url shown below in the picture.',
           line4:'3. Now click on the download button and your video will be downloaded in few seconds.',
           image1:'/images/videos/vq1p1.png',
           image2:'/images/videos/vq1p2.png',
           image3:'/images/videos/vq1p3.png'
        }
    },
    {   id:'2',
        question:'Q2. How can I save videos in my gallery from the local storage of Rocks Player?',
        answer:{  line1:' Whatever the videos you have downloaded through Rocks Video Player will be saved automatically on your Phones Gallery.'}
    },
    // {  id:'3',
    //     question:'Q3.While watching a video, why I\'m facing a sound issue?',
    //     answer:{  }
    // }, 
    {   id:'4',
        question:'Q3. How can I learn about Gesture Control in Rocks Player?',
        answer:{  image1:'/images/videos/vq3P1.png'}
    },
    {  id:'5',
        question:'Q4. Why there is a Buffering issue in online videos?',
        answer:{  line1:' There might be an internet connectivity issue and if it\'s not like that you may try these ways to solve the issues',
               line2:'1. Switch Decoders',
               line3:'2. Turn off your mobile data and restart your phone and again open Rocks Player App.',
               image1:'/images/videos/vq4P1.png'
    }
    },
    {   id:'6',
        question:'Q5. How can I minimize the video full screen?',
        answer:{  line1:'1. Click on the minimize icon.',
              image1:'/images/videos/vq5P1.png'
    }
    },
    {   id:'7',
        question:'Q6. Is there any Equalizer in rocks player so that I can manage sound according to my choice?',
        answer:{  line1:'Yes Rocks Player support Equalizer.',
     image1:'/images/videos/vq6P1.png'
    }
    },
    {   id:'8',
        question:'Q7. How to check the Subtitle option in Rocks Player?',
        answer:{  line2:'1. Click the icon as shown in the picture.',
    line3:'2. Now you can search online Subtitles and Offline according to your choice.',
    image1:'/images/videos/vq7P1.png',
    image2:'/images/videos/vq7P2.png'
    }
    },
    {   id:'9',
        question:'Q8. How can I play video in the background as an MP3?',
        answer:{  line2:'1. Click on the Background Music icon.',
    image1:'/images/videos/vq8P1.png'
    }
    },
    {    id:'10',
        question:'Q9. Is it possible to take a screenshot of any part of a video while the video is running on screen?',
        answer:{  line2:'1. Click on Screenshot icon.',
    image1:'/images/videos/vq9P1.png'
    }
    },
    // {   id:'11',
    //     question:'Q.10 How to manage full screen in the popup videos?',
    //     answer:{  line3:''}
    // },
    {   id:'12',
        question:'Q10. How can I turn on/off Auto-Rotation?',
        answer:{  line1:'1. Click on Rotation icon to on/off Auto-Rotation.',
    image1:'/images/videos/vq11P1.png'
    }
    },
    {   id:'13',
        question:'Q11. Why am I not able to download youtube videos from Rocks Video Player?',
        answer:{  line1:' Sorry to say that but due to legal restriction we are unable to support youtube videos downloads. Here you can only watch youtube videos.'}
    },
    {   id:'14',
        question:'Q12. Why am I failed to play the next video continuously?',
        answer:{  line1:' If you open  the video from your phone or file manager the Rocks Video player dosen\'t play the next video until you select the next video inside the Rocks Player.'}
    },
    {   id:'15',
        question:'Q13. How can I change the color of the Subtitle in the video?',
        answer:{  line2:' Click the Subtitle Customization option as shown in the picture.',
          image1:'/images/videos/vq14P1.png'
    }
    },
    {   id:'16',
        question:'Q14. How can I switch from S/W to H/W   decoder while the video is running on screen?',
        answer:{  line2:'1. Click on the three dot.',
    line3:'2. Click on the Decoder option.',
    line4:'3. Now switch from S/W to H/W decoder.',
    image1:'/images/videos/vq15P1.png',
    image2:'/images/videos/vq15P2.png',
    image3:'/images/videos/vq15P3.png'
    }
    },
    {   id:'17',
        question:'Q15. Why am I not able to manage the playback speed of the video?',
        answer:{  line1:' Rocks Player support Playback Speed feature of the Video player. You can manage the playback speed directly from player\'s screen as shown below.',
    image1:'/images/videos/vq16P1.png'
    }
    },

]
const Video = () => {
    const history=useHistory()
    return (
        <div>
            {data.map((item)=>{
                return (
                  <Container key={item.id} onClick={()=>history.push({pathname:'/videoanswer',state:{question:item.question,answer:item.answer,id:item.id}})}>
                      <Questions>{item.question}</Questions>
                      <img src="/images/next.svg" alt="" width={15}/>
                  </Container>
                )
            })}
        </div>
    )
}

export default Video

const Questions=styled.div`
padding: 10px;
@media (max-width:768px){
    line-height:1.7;
    font-size:15px;
}
@media (max-width:468px){
    line-height:1.5;
    font-size:13px;
}
`
const Container=styled.div`
display: flex;
align-items: center;
justify-content: space-between;
width: 100%;
cursor: pointer;
`