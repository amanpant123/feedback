import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
const data=[
    {  id:"1",
       question:'Q1. Why I am not able to open the page\'s link in the Rocks Video Player\'s browser?',
       answer:{
           line1:"You can refresh the site and please copy the link and paste it into another browser to check if it can open.If not the page may be block or the link is wrong.In that case yu can contact the website owner",
           line2:'If these is not the issue than by these ways.',
           line3:"1. Clear data and cache.",
           line4:"2. Check your mobile network speed or try another wifi network.",
           line5:'If the problem is not solved please send us feedback.'
       }
    },
    {   id:'2',
        question:'Q2. Why does browser speed seems to be slow?',
        answer:{
            line1:'You may clear the cache and data of your Rocks Video Player. Then again copy the URl and paste it into the browser and still you find any issue kindly check your mobile network.'
        }
    },
    {   id:"3",
        question:'Q3. How can i share links directly from the browser?',
        answer:{
            line2:'1. Tap the three dots on the top right corner of the screen as shown below. ',
            line3:"2. Click on share link option as shown in the picture and now share through the selected app as shown below.",
            image1:'/images/browser/BIQ3P1.png',
            image2:"/images/browser/BIQ3P2.png"
        }
    },
    {   id:"4",
        question:'Q4. How to add a new tab if we still want to keep the previous data on the browser window?',
        answer:{
            line2:'1. Click to the top right corner of the screen and tab to new tab option as shown below.',
            line3:"2. Paste the URL of the data you want to search or open it in the browser.",
            line4:"3. Now you will be able to run another site on new tab.",
            image1:"/images/browser/BIQ4P1.png",
            image2:"/images/browser/BIQ4P2.png"
        }
    },
    {   id:"5",
        question:'Q5. How can i enable the Incognito tab in the browser?',
        answer:{
            line1:'Sorry for the inconvenience.',
            line2:'But the Rocks Video Player doesn\'t support the Incognito tab. But our team is working on it and surely you will find it in upcoming version.',
            line3:'If you have any queries please send us feedback.'
        }
    }
]

const Browser = () => {
    const history=useHistory()
    return (
       
        <div>
            {data.map((item)=>{
                return (
                  <Container key={item.id} onClick={()=>history.push({
                      pathname:'/browseranswer',
                      state:{question:item.question,answer:item.answer,id:item.id}
                  })}>
                      <Questions>{item.question}</Questions>
                      <img src="/images/next.svg" alt="" width={15}/>
                  </Container>
                )
            })}
        </div>
    )
}

export default Browser
const Questions=styled.div`
padding: 10px;
line-height:1.3;
@media (max-width:768px){
    line-height:1.7;
    font-size:15px;
}
@media (max-width:468px){
    line-height:1.4;
    font-size:13px;
}
`
const Container=styled.div`
display: flex;
align-items: center;
justify-content: space-between;
cursor: pointer;
`