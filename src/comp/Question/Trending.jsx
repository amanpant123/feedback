import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
const data=[
    {  id:"1",
       question:'Q1. How can I search for different video categories on Rocks Player trending video site?',
       answer:{
           line2:"1. Click on Online icon",
           line3:'2. Click on the Online Video option and then you will be able to search for videos of your choice.',
           image1:"/images/trending/tvq1P1.png",
           image2:"/images/trending/tvq1P2.png"
       }
    },
    {   id:"2",
        question:'Q2. Want to create a playlist of your favorite searches on the Trending Video site?',
        answer:{line2:"1. Tap on the Heart shaped icon placed on the side of searched videos as shown in the picture.",
        line3:'2. Now selected videos will be saved on your favourate automatically',
            image1:"/images/trending/tvq2P1.png",
            image2:"/images/trending/tvq2P2.png"
        }
    },
    {   id:"3",
        question:'Q3. Is it possible to set a perticular country on the Trending video site?',
        answer:{
            line1:'Yes, you can choose the Country of your choice. ',
            line2:"1.Click on Three Dot in Online Video",
            image1:'/images/trending/tvq3P1.png'
        }
    },
    {   id:'4',
        question:'Q4. How can i share a video directly from Trending video site?',
        answer:{
            line2:'1. Tap to the Share icon as shown in the picture below. ',
            line3:"2. Then you can directly share your videos easily",
            image1:"/images/trending/tvq4P1.png",
            image2:"/images/trending/tvq4P2.png"
        }
    }, { id:"5",
        question:'Q5. Sometimes why is it asking for a login for downloading any videos/photos?',
        answer:{
            line1:'No, it will be restored automatically once yu\'ll install the app again.'
        }
    },
]

const Trending = () => {
    const history=useHistory()
    return (
        <div>
            {data.map((item)=>{
                return (
                  <Container key={item.id} onClick={()=>history.push({
                      pathname:'/trendinganswer',
                      state:{question:item.question,answer:item.answer,id:item.id}
                  })}>
                      <Questions>{item.question}</Questions>
                      <img src="/images/next.svg" alt="" width={15}/>
                  </Container>
                )
            })}
        </div>
    )
}

export default Trending
const Questions=styled.div`
padding: 10px;
@media (max-width:768px){
    line-height:1.7;
    font-size:15px;
}
@media (max-width:468px){
    line-height:1.5;
    font-size:13px;
}
`
const Container=styled.div`
display: flex;
align-items: center;
cursor: pointer;
justify-content: space-between;
`