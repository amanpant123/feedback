import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
const data=[
    {  id:"1",
       question:'Q1. How do I change the Theme of the Player while listening to any song?',
       answer:{
           line1:'You can easily change the Theme by following these steps:',
           line2:'1. Click on the Theme icon',
           line3:'2. Click on the use button',
           image1:'/images/audio/Aq1P1.png',
           image2:'/images/audio/Aq1P2.png',
           answer2:{
               line1:'1. Go to Hamburger and click on Theme button',
               line2:'2. Click on Player Theme Banner.',
               image1:'/images/audio/Aq1P3.png',
               image2:'/images/audio/Aq1P4.png',
           }
       }
    },
    {    id: '2',
        question:'Q2. Is it possible to stop the playlist automatically after some time?',
        answer:{
            line1:'You can set the Sleep Timer and automatically your playlist will be stoped.',
            line2:'Click on Sleep icon',
            image1:'/images/audio/Aq2P1.png'
        }
    },
    {   id:"3",
        question:'Q3. How can I change the sequence of songs in the playlist?',
        answer:{
            line1:'You can tap here and change the sequence accordingly',
            line2:'Drag the song and place where you want.',
            image1:'/images/audio/Aq3P1.png'
        }
    },
    {   id:'4',
        question:'Q4. Why am I not able to get different themes on the player screen?',
        answer:{
            line1:'Few Themes are available but for lot more Themes you can take a Premium membership and enjoy the various Themes available.'
        }
    },
    {   id:'5',
        question:'Q5. Is it possible to share a song directly from the Player\'s screen?',
        answer:{
            line1:'Yes you can share it easily with your friends',
            line2:'1. Click on three dot',
            line3:'2. Click Share icon',
            image1:'/images/audio/Aq5P1.png',
            image2:'/images/audio/Aq5P2.png'
        }
    },
]

const Audio = () => {
    const history=useHistory()
    return (
        <div>
            {data.map((item)=>{
                return (
                  <Container key={item.id} onClick={()=>history.push(
                      {pathname:'/audioanswer',
                      state:{question:item.question,answer:item.answer,id:item.id}})}>
                      <Questions>{item.question}</Questions>
                      <img src="/images/next.svg" alt="" width={15}/>
                  </Container>
                )
            })}
        </div>
    )
}

export default Audio
const Questions=styled.div`
padding: 10px;
@media (max-width:768px){
    line-height:1.7;
    font-size:15px;
}
@media (max-width:468px){
    line-height:1.5;
    font-size:13px;
}
`
const Container=styled.div`
display: flex;
align-items: center;
justify-content: space-between;
cursor: pointer;
`