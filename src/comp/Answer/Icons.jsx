import React,{useEffect, useState} from 'react'
import styled from 'styled-components'
import {addDoc,collection} from 'firebase/firestore'
import {AiOutlineClose} from 'react-icons/ai'
import { db } from '../../firebase'
import Swal from 'sweetalert2'
const Icon = ({question}) => {
    const [happy,setHappy]=useState(false)
    const [sad,setSad]=useState(false)
    const [openForm,setOpenForm]=useState(false)
    const [name,setName]=useState('')
    const [email,setEmail]=useState('')
    const [detail,setDetail]=useState('')
    const [validate,setValidate]=useState(false)
    const [submit,setSubmitted]=useState(false)

    useEffect(()=>{
        if(happy){
            addDoc(collection(db,'feedbackfaQ'),{
                question:question,
                happyfeed:'Satisfied'
            }).catch(error=>console.log(error))
        }else if(sad){
            addDoc(collection(db,'feedback'),{
                question:question,
                sadfeed:'Unsatisfied'
            }).catch(error=>console.log(error))
        }else{
            return ;
        }
    },[happy,sad,question])


   const handleSubmit=(e)=>{
       e.preventDefault()
       if(name.length>0&&email.length>0&&detail.length>0){
          addDoc(collection(db,'form'),{
              name:name,
              email:email,
              detail:detail
          }).then(()=>{
              setSubmitted(true)
              setOpenForm(false)
              setName('')
              setEmail('')
              setDetail('')
            //   alert('Feedback Submitted Succesfully')
            Swal.fire({
                maxWidth:'500px',
                text:'Feedback Submitted Succesfully',
                background:'rgba(255,255,255,0.8)',
                color:"black",
                confirmButtonColor:'green',
                showLoaderOnConfirm:true,
                
            })
                    
            }).catch(error=>console.log(error))

        // setDoc(doc(db,"user",email),{
        //     name:name,
        //     email:email,
        //     detail:detail
        // }).then(()=>setOpenForm(false)).catch(error=>console.log(error))

       }else{
           setValidate(true)
       }
   }


    return (
        <IconContainer>
            {openForm?(
                <FormContainer  onSubmit={handleSubmit}>
                    <AiOutlineClose1 size="20px" style={{color:'red'}} onClick={()=>setOpenForm(false)} />
                    <NameInput onChange={(e)=>setName(e.target.value)} value={name} placeholder="NAME"/>
                    <EmailInput type="email" onChange={(e)=>setEmail(e.target.value)} value={email} placeholder="EMAIL"/>
                    <DetailInput onChange={(e)=>setDetail(e.target.value)} value={detail} placeholder="FEEDBACK"/>
                    {validate&&(
                        <h3>All fields are required</h3>
                    )}
                    <SubmitButton submit={submit} disabled={submit} type="submit">
                        Submit
                    </SubmitButton>
                </FormContainer>
            ):(<>
                <IconTitle>
                Is this  answer helpfull ?
            </IconTitle>
            <Icons>
                <IconImage onClick={()=>{
                    setHappy(!happy);
                    setSad(false);

                }} src={happy?'images/happy.svg':'images/sad_happy_happy.svg'} happy={happy}/>
                <IconImage onClick={()=>{
                    setSad(!sad);
                    setHappy(false);
                }} src={sad?'images/sad.svg':'images/sad_happy_sad.svg'}/>
            </Icons>
            <IconFooter>
                <h3 style={{color:submit?'green':'red',marginTop:20,cursor:'pointer'}} onClick={()=>setOpenForm(true)}>{submit?'Thank you ':'Send us feedback'}</h3>
            </IconFooter>
            </>
            )}
            
            </IconContainer>
    )
}
export default Icon

const IconContainer=styled.div`
margin: auto;
width: 100%;
padding: 20px;
border-top: 1px solid red;
margin-top: 100px;
@media (max-width:768px){
    margin-top: 5px;
    width: 100%;
    text-align: center;
    margin-left: 0px;
}
`
const IconTitle=styled.h4`
font-family: sans-serif;
font-size: 30px;
margin-top:30px;
align-items: center;
text-align: center;
@media (max-width:768px){
    font-size: 25px;
}
@media (max-width:468px){
    font-size: 20px;
}
`
    
const Icons=styled.div`
display: flex;
justify-content: space-around;
width: 30%;
padding: 20px;
margin: auto;
margin-top: 20px;
align-items: center;
@media (max-width:768px){
    margin-top: 10px;
    justify-content: space-between;
    width: 50%;
}
`
const IconFooter=styled.div`
padding: 20px;
margin: auto;
text-align: center;
margin-bottom:40px;
`
const IconImage=styled.img`
width: 100px;
height: 100px;
cursor: pointer;
@media (max-width:768px){
    width: 160px;
    height: 50px;
}
`
const FormContainer=styled.form`
display: flex;
flex-direction: column;
justify-content: space-between;
align-items: center;
max-height: 250px;
height: 250px;
position: relative;
margin-bottom:100px;
margin-top:50px;
`
const NameInput=styled.input`
padding:10px;
border:none;
outline: none;
border-radius: 20px;
width: 20%;
color: white;
background-color:rgba(255,255,255,0.1);
&::placeholder{
    color:white;
}
@media(max-width:768px){
    width: 60%;
}
@media(max-width:1200px){
    width: 40%;
}
@media(max-width:468px){
    width: 80%;
}
`
const EmailInput=styled.input`
padding:10px;
background-color:rgba(255,255,255,0.1);
border: none;
&::placeholder{
    color:white;
}
outline: none;
border-radius: 20px;
width: 20%;
color: white;
@media(max-width:768px){
    width: 60%;
}
@media(max-width:1200px){
    width: 40%;
}
@media(max-width:468px){
    width: 80%;
}
`
const DetailInput=styled.textarea`
padding:10px;
border: none;
outline: none;
resize:none;
height:50px;
background-color:rgba(255,255,255,0.1);
&::placeholder{
    color:white;
}
border-radius: 10px;
width: 20%;
color: white;
@media(max-width:768px){
    width: 60%;
}
@media(max-width:1200px){
    width: 40%;
}
@media(max-width:468px){
    width: 80%;
}
`

const SubmitButton=styled.button`
background-color:${props=>props.submit?'red':'red'};
padding: 10px;
width: 20%;
border: none;
outline: none;
border-radius: 35px;
transition-duration: 0.3s;
font-size: 15px;
&:hover{
    transform: ${props=>props.submit?'scale(1.0)':'scale(1.05)'};
}
@media(max-width:768px){
    width: 60%;
}
@media(max-width:1200px){
    width: 40%;
}
@media(max-width:468px){
    width: 80%;
}
`


const AiOutlineClose1=styled(AiOutlineClose)`
position: absolute;
right: 35%;
cursor: pointer;
top: 5px;
@media(max-width:1208px){
    right: 25%;
}
@media(max-width:768px){
    right: 17%;
}
@media(max-width:468px){
    position: static;
}
`